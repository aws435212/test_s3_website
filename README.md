# test_s3_website


![Homepage](captures/index.png)
![Page d'erreur](captures/error.png)

# Configuration d'un site web statique sur AWS S3

Ce guide vous montrera comment configurer un site web statique hébergé sur Amazon S3 (Simple Storage Service) en utilisant AWS Management Console.

## Étapes de configuration :

### 1. Créer un bucket S3 :

- Connectez-vous à [AWS Management Console](https://console.aws.amazon.com/).
- Accédez au service S3.
- Cliquez sur "Create bucket" (Créer un bucket).
- Entrez un nom unique pour votre bucket (par exemple, "mon-site-web").
- Sélectionnez la région AWS pour votre bucket.
- Cliquez sur "Create" (Créer) pour créer le bucket.

### 2. Activer l'hébergement statique :

- Dans le tableau de bord du bucket S3, cliquez sur l'onglet "Properties" (Propriétés).
- Faites défiler vers le bas jusqu'à la section "Static website hosting" (Hébergement de site web statique).
- Sélectionnez "Use this bucket to host a website" (Utiliser ce bucket pour héberger un site web).
- Entrez le nom du fichier d'index (par exemple, "index.html") et, éventuellement, une page d'erreur de redirection.
- Cliquez sur "Save" (Enregistrer) pour activer l'hébergement statique.

### 3. Configuration des autorisations :

- Dans le tableau de bord du bucket S3, cliquez sur l'onglet "Permissions" (Autorisations).
- Assurez-vous que le bucket est accessible au public en configurant les politiques d'accès. Vous pouvez par exemple définir une politique d'accès qui permet l'accès à tout le monde (consultez la documentation AWS pour les détails).
- Mettez à jour les paramètres CORS (Cross-Origin Resource Sharing) si nécessaire pour autoriser les demandes cross-origin.

### 4. Télécharger votre contenu :

- Téléchargez les fichiers de votre site web (par exemple, HTML, CSS, JavaScript) dans votre bucket S3 en utilisant l'interface de gestion de S3 ou en utilisant l'AWS CLI.
- Assurez-vous que les fichiers ont les autorisations appropriées pour être consultés publiquement.

### 5. Accès au site web :

- Une fois que les fichiers sont téléchargés, accédez à votre site web en utilisant le domaine endpoint fourni par S3 ou en utilisant un nom de domaine personnalisé associé à CloudFront si vous utilisez un CDN.

## Remarques :

- Assurez-vous de surveiller les coûts associés à l'utilisation de S3 pour l'hébergement de votre site web.
- Consultez la documentation AWS pour plus d'informations sur la configuration avancée et les bonnes pratiques.
